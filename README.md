# Client MesVaccins en NodeJS

Exemple d'implémentation de l'API professionnelle [MesVaccins.net](http://www.mesvaccins.net) utilisant NodeJS.

Cette implémentation est fournie à titre d'exemple uniquement.

## Configuration de l'application

Pour faire fonctionner ce projet, il est au préalable nécessaire de renseigner les variables d'environnement suivantes :

- ORGANISATION_ID
- OTP
- UID
- SECRET

## License

    The MIT License (MIT)

    Copyright (c) 2015 Josselin Auguste / Syadem

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

## Manual d'intégration

Un manuel d'intégration décrit les fonctionnalités de notre API et les modalités de leur utilisation : [https://www.mesvaccins.net/manual/](https://www.mesvaccins.net/manual/).

## Nous contacter

Vous pouvez joindre le support développeurs de MesVaccins.net à [developers@mesvaccins.net](mailto:developers@mesvaccins.net).

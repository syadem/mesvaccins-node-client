const fs = require("fs");
const bcrypt = require("bcrypt");
const https = require("https");

const authentifier = "monemail@organisation.fr";
const organisationId = process.env.ORGANISATION_ID;
const UID = process.env.UID; 
const secret = process.env.SECRET;
const otp = process.env.OTP;
const authURL = "https://test-pro-secure.mesvaccins.net/authenticationids";

const hash = bcrypt.hashSync(authentifier + organisationId, "$2a$10$" + UID);
console.log(`Password: ${hash}`);

// node
var nodeOptions = {
    host: 'test-pro-secure.mesvaccins.net',
    port: 443,
    path: '/authenticationids/restloginservice.php',
    method: 'POST',
    key: fs.readFileSync('cert/mesvaccinsnet.key'),
    cert: fs.readFileSync('cert/mesvaccinsnet.crt'),
    passphrase: otp,
    requestCert: true,
    rejectUnauthorized: true,
    headers: {
      'Content-Type': "application/json; charset=utf-8"
    }
};

var req = https.request(nodeOptions, function(res) {
    res.on('data', function(d) {
        process.stdout.write(d);
    });
});

const data = `{"authentifier": "${authentifier}", "password": "${hash}"}`;
req.write(data);

req.on('error', function(e) {
    console.error(e);
});

req.end();
